import kz.jmart.bst.BSTCustom;
import kz.jmart.bst.TreeNode;

public class Main {
    public static void main(String[] args) {
        BSTCustom bst = new BSTCustom();
        TreeNode root = null;
        root = bst.insert(root, 5);
        root = bst.insert(root, 3);
        root = bst.insert(root, 7);
        root = bst.insert(root, 1);
        root = bst.insert(root, 4);
        root = bst.insert(root, 6);
        root = bst.insert(root, 8);

        System.out.print("In order traversal of bst "); bst.inOrder(root);
        System.out.println();
        System.out.print("Level order traversal of bst "); bst.levelOrder(root);
        System.out.println();
        System.out.print("Reverse order traversal of bst "); bst.reverseOrder(root);

        System.out.println(String.format("\nDoes 1 in BST %b", bst.search(root, 1)));
        System.out.println(String.format("Does 7 in BST %b", bst.search(root, 7)));
        System.out.println(String.format("Does 11 in BST %b", bst.search(root, 11)));

        System.out.println(String.format("Depth of the tree is %d", bst.depth(root)));
        System.out.println(String.format("Height of the tree is %d", bst.height(root)));
    }
}